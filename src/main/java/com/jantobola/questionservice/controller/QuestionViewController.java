package com.jantobola.questionservice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class QuestionViewController {

    @GetMapping
    public String showAddQuestionPage() {
        return "question-add.html";
    }

    @GetMapping("/list")
    public String showQuestionList() {
        return "question-list.html";
    }

}
