package com.jantobola.questionservice.controller;

import com.jantobola.questionservice.model.QuestionRequest;
import com.jantobola.questionservice.model.QuestionResponse;
import com.jantobola.questionservice.service.QuestionService;
import lombok.RequiredArgsConstructor;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService questionService;

    @PostMapping
    public ResponseEntity<Void> postQuestion(@RequestBody QuestionRequest request) {
        questionService.saveQuestion(request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<QuestionResponse>> getQuestions() {
        return ResponseEntity.ok(questionService.getQuestions());
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteAll() {
        questionService.deleteAll();
        return ResponseEntity.noContent().build();
    }

}
