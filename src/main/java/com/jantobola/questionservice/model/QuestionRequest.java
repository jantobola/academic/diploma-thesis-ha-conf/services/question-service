package com.jantobola.questionservice.model;

import lombok.Data;

@Data
public class QuestionRequest {

    private String name;
    private String question;

}
