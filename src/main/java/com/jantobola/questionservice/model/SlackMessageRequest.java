package com.jantobola.questionservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Data
@ToString
@AllArgsConstructor
public class SlackMessageRequest {

    @NotBlank
    private String title;

    @NotBlank
    private String message;

}
