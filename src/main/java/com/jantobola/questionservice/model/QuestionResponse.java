package com.jantobola.questionservice.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.ZonedDateTime;

@Data
@EqualsAndHashCode(callSuper = true)
public class QuestionResponse extends QuestionRequest {

    private ZonedDateTime addedAt;

}
