package com.jantobola.questionservice.configuration;

import com.jantobola.questionservice.messaging.SlackChannel;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@EnableBinding(SlackChannel.class)
@Configuration
public class StreamsConfiguration {
}
