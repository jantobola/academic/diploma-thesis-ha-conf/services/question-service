package com.jantobola.questionservice.messaging;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface SlackChannel {

    String OUTPUT = "slack-channel-out";

    @Output(OUTPUT)
    MessageChannel outboundSlackChannel();

}
