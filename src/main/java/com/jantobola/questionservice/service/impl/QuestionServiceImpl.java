package com.jantobola.questionservice.service.impl;

import com.jantobola.questionservice.domain.Question;
import com.jantobola.questionservice.messaging.SlackChannel;
import com.jantobola.questionservice.model.QuestionRequest;
import com.jantobola.questionservice.model.QuestionResponse;
import com.jantobola.questionservice.model.SlackMessageRequest;
import com.jantobola.questionservice.repository.QuestionRepository;
import com.jantobola.questionservice.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MimeTypeUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class QuestionServiceImpl implements QuestionService {

    private final QuestionRepository questionRepository;
    private final SlackChannel slackChannel;

    @Override
    public List<QuestionResponse> getQuestions() {
        return questionRepository.findAll().stream()
                .map(q -> {
                    var r = new QuestionResponse();
                    r.setAddedAt(q.getAddedDateTime());
                    r.setName(q.getName());
                    r.setQuestion(q.getQuestion());
                    return r;
                }).collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void saveQuestion(QuestionRequest question) {
        var q = new Question();
        q.setAddedDateTime(ZonedDateTime.now(ZoneOffset.UTC));
        q.setName(question.getName());
        q.setQuestion(question.getQuestion());
        questionRepository.save(q);

        log.info("New question saved, sending to messaging slack channel.");

        var message = new SlackMessageRequest(q.getName(), q.getQuestion());
        slackChannel.outboundSlackChannel().send(MessageBuilder
                .withPayload(message)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build());
    }

    @Override
    public void deleteAll() {
        questionRepository.deleteAll();
    }
}
