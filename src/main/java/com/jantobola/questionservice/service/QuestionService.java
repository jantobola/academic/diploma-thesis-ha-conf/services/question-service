package com.jantobola.questionservice.service;

import com.jantobola.questionservice.model.QuestionRequest;
import com.jantobola.questionservice.model.QuestionResponse;

import java.util.List;

public interface QuestionService {

    List<QuestionResponse> getQuestions();

    void saveQuestion(QuestionRequest question);

    void deleteAll();

}
