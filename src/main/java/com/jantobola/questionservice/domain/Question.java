package com.jantobola.questionservice.domain;

import lombok.Data;

import javax.persistence.*;

import java.time.ZonedDateTime;

@Data
@Entity
public class Question {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Lob
    private String question;
    private ZonedDateTime addedDateTime;

}
